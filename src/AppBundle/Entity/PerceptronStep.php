<?php
/**
 * Created by PhpStorm.
 * User: mistrz
 * Date: 08.01.19
 * Time: 19:35
 */

namespace AppBundle\Entity;


class PerceptronStep
{

//    epoka
    private $e;

//    czas
    private $t;

//    tablica x-ow,d i wag
    private $x;
    private $d;
    private $w;


//    wartosc s
    private $s;

//    wartosc y
    private $y;
//    jaka funkcja aktywacji
    private $isUni;

    private $isOk;


    private $tablicaDanychDlaOsi=null;

    public function __construct($e,$t,$x,$d,$w,$isUni)
    {
        $this->e=$e;
        $this->t=$t;
        $this->x=$x;
        $this->d=$d;
        $this->w=$w;
        $this->isUni=$isUni;
    }

    public function obliczDaneDlaOsi(){
        if ($this->w[2]==0.0){
            if($this->w[1]!=0.0){
                $this->tablicaDanychDlaOsi=[
                    [
                        'x'=>-5,
                        'y'=>-1*($this->w[0]/$this->w[1]),
                    ],
                    [
                        'x'=>5,
                        'y'=>-1*($this->w[0]/$this->w[1]),
                    ]
                ];
            }else{
                $this->tablicaDanychDlaOsi=null;
            }
        }else{
            $this->tablicaDanychDlaOsi=[
                [
                    'x'=>-10,
                    'y'=>(-1*($this->w[0]/$this->w[2])-($this->w[1]/$this->w[2]*(-10))),
                ],
                [
                    'x'=>10,
                    'y'=>-1*($this->w[0]/$this->w[2])-($this->w[1]/$this->w[2]*(10))
                ]
            ];

        }
    }

    public function oblicz(){
        $this->funkcjaS();
        $this->funkcjaAktywacji();
        $this->czyIsOk();
        return $this->getisOk();
    }

    private function funkcjaS(){
        $this->s=0;
        for ($i=0;$i<count($this->x);$i++){
            $this->s+=$this->x[$i]*$this->w[$i];
        }
    }

    private function funkcjaAktywacji(){
        $this->y=($this->s>0)?1:($this->isUni?0:-1);

    }

    private function czyIsOk(){
        $this->isOk=$this->y==$this->d?true:false;
    }

    /**
     * @return mixed
     */
    public function getE()
    {
        return $this->e;
    }

    /**
     * @param mixed $e
     */
    public function setE($e)
    {
        $this->e = $e;
    }

    /**
     * @return mixed
     */
    public function getT()
    {
        return $this->t;
    }

    /**
     * @param mixed $t
     */
    public function setT($t)
    {
        $this->t = $t;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return mixed
     */
    public function getD()
    {
        return $this->d;
    }

    /**
     * @param mixed $d
     */
    public function setD($d)
    {
        $this->d = $d;
    }

    /**
     * @return mixed
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * @param mixed $w
     */
    public function setW($w)
    {
        $this->w = $w;
    }

    /**
     * @return mixed
     */
    public function getS()
    {
        return $this->s;
    }

    /**
     * @param mixed $s
     */
    public function setS($s)
    {
        $this->s = $s;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @return boolean
     */
    public function getisUni()
    {
        return $this->isUni;
    }

    /**
     * @param boolean $isUni
     */
    public function setIsUni($isUni)
    {
        $this->isUni = $isUni;
    }

    /**
     * @return boolean
     */
    public function getisOk()
    {
        return $this->isOk;
    }

    /**
     * @param boolean $isOk
     */
    public function setIsOk($isOk)
    {
        $this->isOk = $isOk;
    }

    /**
     * @return array|null
     */
    public function getTablicaDanychDlaOsi()
    {
        return $this->tablicaDanychDlaOsi;
    }

    /**
     * @param array $tablicaDanychDlaOsi
     */
    public function setTablicaDanychDlaOsi($tablicaDanychDlaOsi)
    {
        $this->tablicaDanychDlaOsi = $tablicaDanychDlaOsi;
    }



}