<?php
/**
 * Created by PhpStorm.
 * User: mistrz
 * Date: 08.01.19
 * Time: 16:52
 */

namespace AppBundle\Entity;


class Perceptron
{
//    zlicza ile razy pod rzad byly poprawne perceptriony

    /**
     * @var integer
     */
    private $licznikPoprawnychPerceptronow;

//    jezeli siec skonczyla dzialanie ustawia sie na true

    /**
     * @var boolean
     */
    private $isKoniec=false;


    /**
     * @var double
     */
    private $p=0.5;

    /**
     * @var integer
     */
    private $t=0;


    private $x=array();

    private $w=array();

    private $d=array();

    private $steps=array();

    /**
     * @var double
     */
    private $a;
    /**
     * @var double
     */
    private $b;

    /**
     * @var integer
     */
    private $n;

    /**
     * @var boolean
     */
    private $isUnipolarna=false;

    public function __construct($data)
    {

        $this->licznikPoprawnychPerceptronow=0;
        $this->n=intval($data["n"]);


//        p
        $this->p=doubleval($data["p"]);

//        a i b
        $this->a=doubleval($data["a"]);
        $this->b=doubleval($data["b"]);
        if (isset($data["isUnipolarna"]) and $data["isUnipolarna"]=="on"){
            $this->isUnipolarna=true;
        }else{
            $this->isUnipolarna=false;
        }


//        tablice W
        if ($data["w0"]!="" and $data ["w1"]!="" and $data["w2"]!=""){
            $this->w=array(doubleval($data["w0"]),doubleval($data["w1"]),doubleval($data["w2"]));
        }else{
            $this->generujTabliceW(doubleval($data["zw0"]),doubleval($data["zw1"]));
        }

//        Tablice X
        $this->generujTabliceX();
        $this->generujTabliceD();

    }


    public function nextStep(){

        $count=count($this->steps);

        if ($count>0){
            // generujemy nowa tablice jezeli poprzedni krok wygenerowal zly krok
            if (!$this->steps[$count-1]->getIsOk()){
                $this->generujNowaTabliceW($this->steps[$count-1]);
            }
        }
        /** @var PerceptronStep $newStep */
        $newStep=new PerceptronStep($this->getEpoka(),$this->getT(),$this->getXodT(),$this->getDodT(),$this->getW(),$this->isUnipolarna);
        if ($newStep->oblicz()){
            $this->licznikPoprawnychPerceptronow++;
            if ($this->licznikPoprawnychPerceptronow>=$this->n){
                $this->isKoniec=true;
            }
        }else{
            $this->licznikPoprawnychPerceptronow=0;
        }

        if ($count>0){
            if (!$this->steps[$count-1]->getIsOk()){
                $newStep->obliczDaneDlaOsi();
            }
        }else{
            $newStep->obliczDaneDlaOsi();
        }



        $this->steps[]=$newStep;
        $this->t++;

        return $newStep;
    }

    private function generujTabliceX(){
        for ($i=0;$i<$this->n;$i++){
            $temp=array(1,rand(-10,10),rand(-10,10));
            $this->x[]=$temp;
        }
    }

    private function generujTabliceW($wMin,$wMax){
        for ($i=0;$i<3;$i++){
            $temp=rand($wMin,$wMax);
            $this->w[]=$temp;
        }
    }

    private function generujTabliceD(){
        foreach ($this->x as $item){
            if ($item[2]>$this->a*$item[1]+$this->b){
                $this->d[]=1;
            }else{
                $this->d[]=$this->isUnipolarna?0:-1;
            }
        }
    }


    /**
     * @param PerceptronStep $perceptronStep
     */
    private function generujNowaTabliceW($perceptronStep){

        $tempArrayW=array();
        for ($i=0;$i<count($this->w);$i++){
            $tempArrayW[]=$this->w[$i]+$this->p*($perceptronStep->getD()*1.0 - $perceptronStep->getY()*1.0)*$perceptronStep->getX()[$i];
        }
        $this->w=$tempArrayW;
    }

    public function getLastStep(){
        return $this->steps[count($this->steps)];
    }




    public function getEpoka(){
        return floor($this->t/$this->n)+1;
    }

    /**
     * @return float
     */
    public function getP()
    {
        return $this->p;
    }

    /**
     * @param float $p
     */
    public function setP($p)
    {
        $this->p = $p;
    }

    /**
     * @return int
     */
    public function getT()
    {
        return $this->t;
    }

    public function getTdlaEpoki(){
        return $this->t%$this->n;
    }

    /**
     * @param int $t
     */
    public function setT($t)
    {
        $this->t = $t;
    }

    /**
     * @return array
     */
    public function getX()
    {
        return $this->x;
    }

    public function getXodT(){
        return $this->x[$this->getTdlaEpoki()];
    }

    /**
     * @param array $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return array
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * @param array $w
     */
    public function setW($w)
    {
        $this->w = $w;
    }

    /**
     * @return array
     */
    public function getD()
    {
        return $this->d;
    }

    public function getDodT(){
        return $this->d[$this->getTdlaEpoki()];
    }

    /**
     * @param array $d
     */
    public function setD($d)
    {
        $this->d = $d;
    }

    /**
     * @return array
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param array $steps
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
    }

    /**
     * @return float
     */
    public function getA()
    {
        return $this->a;
    }

    /**
     * @param float $a
     */
    public function setA($a)
    {
        $this->a = $a;
    }

    /**
     * @return float
     */
    public function getB()
    {
        return $this->b;
    }

    /**
     * @param float $b
     */
    public function setB($b)
    {
        $this->b = $b;
    }

    /**
     * @return int
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * @param int $n
     */
    public function setN($n)
    {
        $this->n = $n;
    }

    /**
     * @return bool
     */
    public function isUnipolarna()
    {
        return $this->isUnipolarna;
    }

    /**
     * @param bool $isUnipolarna
     */
    public function setIsUnipolarna($isUnipolarna)
    {
        $this->isUnipolarna = $isUnipolarna;
    }

    /**
     * @return bool
     */
    public function isKoniec()
    {
        return $this->isKoniec;
    }




}