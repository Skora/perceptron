<?php
/**
 * Created by PhpStorm.
 * User: mistrz
 * Date: 08.01.19
 * Time: 18:42
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Perceptron;
use AppBundle\Entity\PerceptronStep;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PerceptronController
 * @package AppBundle\Controller
 * @Route("/perceptron")
 */
class PerceptronController extends Controller
{

    /**
     * @param Request $request
     * @Route("/",name="perceptronPrzygotuj")
     * @return Response
     */
    public function przygotujDane(Request $request){
        $perceptron=null;
        if ($request->request->has('perceptron')){
            $perceptrion=$request->request->get('perceptron');
            /** @var Perceptron $perceptrion */
            $perceptron=unserialize($perceptrion);
        }else{
            $data=$request->request->all();
            $perceptron=new Perceptron($data);
        }

        /** @var PerceptronStep $perceptronStep */
        $perceptronStep=$perceptron->nextStep();
        $perceptronSerialized=serialize($perceptron);

        return new JsonResponse(array(
            'perceptron'=>$perceptronSerialized,
            'tableRow'=>$this->generateTableRowFromPerceptronStep($perceptronStep),
            't'=>$perceptronStep->getT(),
            'os'=>$perceptronStep->getTablicaDanychDlaOsi(),
            'isKoniec'=>$perceptron->isKoniec()
        ));
    }

    private function generateTableRowFromPerceptronStep($perceptronStep){

        $tableRow = $this->render('default/tableBody.html.twig',array(
            'percep'=>$perceptronStep
        ))->getContent();
        return $tableRow;
    }




}