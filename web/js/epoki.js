$.fn.scrollView = function (time) {
    time =time||0;
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, time);
    });
};

var perceptron = {
    objectInstance:null,
    init:function(){

        $('#nextPokolenie').on('click',function () {
            var url=$("#nastepnyKrokGeneruj").val();
            var data={'perceptron':perceptron.objectInstance};

            perceptron.sendPostAjax(url,data);
        });
        $('#konfiguracjaForm').on('submit',function (e) {
            var form = $(this);
            var url=form.attr('action');
            var data=form.serialize();



            e.preventDefault();
            perceptron.sendPostAjax(url,data);


            $('#nextPokolenie').show();
            $('#chartSelect').empty();
            $("#tableBody").empty();


        });

        $(document).on("change",'#chartSelect',function () {
            console.log('Akcja!');
            var tablica=JSON.parse($(this).val());

            console.log(tablica);
            perceptron.generujChart(tablica,500);
        });
    },


    
    sendPostAjax:function (url,ajaxData) {

        $.ajax({
            method:"post",
            url:url,
            data: ajaxData,
            success:function (data) {

                $("#tableEpok").append(data["tableRow"]);

                if (data["os"]!=null){
                    console.log(data);
                    perceptron.generujChart(data["os"]);
                    $("#chartSelect").append("<option value="+JSON.stringify(data['os'])+"> t:"+data["t"]+"</option>");
                }
                perceptron.objectInstance=data["perceptron"];
                if (data["isKoniec"]===true){
                    alert('Skończono nauczanie!');
                    $('#nextPokolenie').hide();
                };
            }
        })
    },

    generujChart:function (tablicaDanych,time) {
        time = time || 0;

        if (Array.isArray(tablicaDanych)){

            var canvas = $('#chart');

            new Chart(canvas, {
                type: 'line',
                data: {
                    datasets: [{
                        label: '',
                        borderColor:'rgb(54, 162, 235)',
                        data: tablicaDanych,

                        // data:
                        // [{
                        //     x: -4,
                        //     y: 1
                        // },{
                        //     x: 4,
                        //     y: 1
                        // }
                        // ],
                        fill: false
                    }]
                },
                options: {
                    animation: {
                        duration: 0, // general animation time
                    },
                    scales: {
                        xAxes: [{
                            type: 'linear',
                            position: 'bottom',
                            ticks:{
                                min: -2,
                                max: 2,
                                stepSize:0.5,
                                stack: true
                            }
                        }],
                        yAxes: [{
                            type: 'linear',
                            position: 'bottom',
                            ticks:{
                                min: -2,
                                max: 2,
                                stepSize:0.5,
                                stack: true
                            }
                        }]
                    }
                }

            });

            console.log("focus");
            $(canvas).scrollView(time);
        }
    }
};

$(document).ready(function () {
   perceptron.init();
});
